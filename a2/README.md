> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Managment

## Adam Michia

### Assignment A2 Requirements:

    -  Create a local database with company and customer tables
    -  Insert two users and complete grant permissions exercises
    -  Execute on both local and torch servers


## A2 Database Business Rules:

Locally: create your fsu id database, and two tables: company and customer

NOTE: Also, these two tables must be populated in yourfsuid database on the CCI server. 

    a. Use 1:M relationship:
        Company is parent table

    b. company attributes:
        i. cmp_id (pk)
        ii. cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership')
        iii. cmp_street
        iv. cmp_city
        v. cmp_state
        vi. cmp_zip (zf)
        vii. cmp_phone
        viii. cmp_ytd_sales
        ix. cmp_url
        x. cmp_notes

    c. customer attributes:
        i. cus_id (pk)
        ii. cmp_id (fk)
        iii. cus_ssn (binary 64)
        iv. cus_salt (binary 64)
        v. cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering')
        vi. cus_first
        vii. cus_last
        viii. cus_street
        ix. cus_city
        x. cus_state
        xi. cus_zip (zf)
        xii. cus_phone
        xiii. cus_email
        xiv. cus_balance
        xv. cus_tot_sales
        xvi. cus_notes

    d. Create suitable indexes and foreign keys:
        *Enforce pk/fk relationship: on update cascade, on delete restrict




#### README.md file includes the following items:

* Screenshot of *your* SQL code;
* Screenshot of *your* populated tables;




#### Assignment Screenshots:

*Screenshot#1 of A2 Customer Table*:

![LIS3781 ERD for Assignment 1 Screenshot](img/customer.png)

*Screenshot#2 of A2 Company Table*:

![LIS3781 Select Statement in Workbench for Assignment 1 Screenshot](img/company.png)

*Screenshot#3 of A2 Customer/Company Populated Tables*:

![LIS3781 Select Statement in Workbench for Assignment 1 Screenshot](img/tables.png)