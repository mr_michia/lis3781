SET define OFF 
DROP SEQUENCE seq_cus_id; -- see Oracle documentation for auto increment 
CREATE SEQUENCE seq_cus_id 
  START WITH 1 
  INCREMENT BY 1 
  MINVALUE 1 
  MAXVALUE 10000; 

DROP TABLE customer CASCADE CONSTRAINTS purge; 

CREATE TABLE customer 
  ( 
     cus_id      NUMBER(3, 0) NOT NULL,-- max value 999 
     cus_fname   VARCHAR2(15) NOT NULL, 
     cus_lname   VARCHAR2(30) NOT NULL, 
     cus_street  VARCHAR2(30) NOT NULL, 
     cus_city    VARCHAR2(30) NOT NULL, 
     cus_state   CHAR(2) NOT NULL, 
     cus_zip     NUMBER(9) NOT NULL,-- equivalent to number(9,0) 
     cus_phone   NUMBER(10) NOT NULL, 
     cus_email   VARCHAR2(100), 
     cus_balance NUMBER(7, 2),-- max value 9999999.99 
     cus_notes   VARCHAR2(255), 
     CONSTRAINT pk_customer PRIMARY KEY(cus_id) 
  ); 

DROP SEQUENCE seq_com_id; -- for auto increment 

CREATE SEQUENCE seq_com_id 
  START WITH 1 
  INCREMENT BY 1 
  MINVALUE 1 
  MAXVALUE 10000; 

DROP TABLE commodity CASCADE CONSTRAINTS purge; 

CREATE TABLE commodity 
  ( 
     com_id    NUMBER NOT NULL, 
     com_name  VARCHAR2(20), 
     com_price NUMBER(8, 2) NOT NULL, 
     cus_notes VARCHAR2(255), 
     CONSTRAINT pk_commodity PRIMARY KEY(com_id), 
     CONSTRAINT uq_com_name UNIQUE(com_name) -- Note: case-sensitive by default! 
  ); 

DROP SEQUENCE seq_ord_id; -- for auto increment 

CREATE SEQUENCE seq_ord_id 
  START WITH 1 
  INCREMENT BY 1 
  MINVALUE 1 
  MAXVALUE 10000; 

-- Demo purposes: Quoted identifiers can be reserved words (e.g., order), although this is *not* recommended

DROP TABLE "order" CASCADE CONSTRAINTS purge; 

CREATE TABLE "order" 
  ( 
     ord_id         NUMBER(4, 0) NOT NULL, -- max value 9999 (permitting only integers, no decimals) 
     cus_id         NUMBER, 
     com_id         NUMBER, 
     ord_num_units  NUMBER(5, 0) NOT NULL, -- max value 99999 (permitting only integers, no decimals) 
     ord_total_cost NUMBER(8, 2) NOT NULL, 
     ord_notes      VARCHAR2(255), 
     CONSTRAINT pk_order PRIMARY KEY(ord_id), 
     CONSTRAINT fk_order_customer FOREIGN KEY (cus_id) REFERENCES customer(cus_id), 
     CONSTRAINT fk_order_commodity FOREIGN KEY (com_id) REFERENCES commodity(com_id), 
     CONSTRAINT check_unit CHECK(ord_num_units > 0), 
     CONSTRAINT check_total CHECK(ord_total_cost > 0) 
  ); 
-- Oracle NEXTVAL function used to retrieve next value in sequence
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Beverly', 'Davis', '123 Main St.', 'Detroit', 'MI', 48252, 3135551212, 'bdavis@aol.com', 11500.99, 'recently moved');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Stephen', 'Taylor', '456 Elm St.', 'St. Louis', 'MO', 57252, 4185551212, 'staylor@comcast.net',25.01, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Donna', 'Carter', '789 Peach Ave.', 'Los Angeles', 'CA', 48252, 3135551212, 'darter@wow.com',300.99, 'returning customer');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Robert', 'Silverman', '857 Wilbur Rd.', 'Phoenix', 'AZ', 25278, 4805551212, 'rsilverman@aol.com',NULL, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Sally', 'Victors', '534 Holler Way', 'Charleston', 'WV', 78345, 9045551212, 'svictors@wow.com',500.76, 'new customer');
commit;
-- Note: Oracle does *not* autocommit by default! DML STATEMENTS WILL ONLY LAST FOR THE SESSION!
-- When forgetting to commit ML statements --for example, with inserts, selecting a table will display "no rows selected"!
INSERT INTO commodity VALUES (seq_com_id.nextval, 'DVD & Player', 109.00, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Cereal', 3.00, 'sugar free');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Scrabble', 29.00, 'original');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Licorice', 1.89, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'tums ', 2.45, ' antacid ');
commit;