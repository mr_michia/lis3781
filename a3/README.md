> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database

## Adam Michia


### Assignment A3 Requirements:

1. Install Oracle login and create accessibility
2. Create and populate tables
3. Type codes and run queries to 25 questions for required reports



#### README.md file includes the following items:

1. Screenshot of *your* SQL code used to create and populate your tables; 
2. Screenshot of *your* populated tables (w/in the Oracle environment); 
3. Optional: SQL code for a few of the required reports. 
4. Bitbucket repo links: *Your* lis3781 Bitbucket repo link 



### Assignment Screenshots:

#### *Screenshot of *your* SQL code used to create and populate your tables*:

![SQL code for Assignment 3 Screenshot](img/code.png)


#### *Screenshot of *your* populated tables (w/in the Oracle environment)*:

-Order Table:
![Order Table in Oracle for Assignment 3 Screenshot](img/order.png)

-Commodity Table:
![Commodity Table in Oracle for Assignment 3 Screenshot](img/commodity.png)

-Customer Table:
![Customer Table in Oracle for Assignment 3 Screenshot](img/customer.png)
