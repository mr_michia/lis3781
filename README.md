> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Management

## Adam Michia

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install MySQL
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions 

2. [A2 README.md](a2/README.md "My A2 README.md file")
    -  Create a local database with company and customer tables
    -  Insert two users and complete grant permissions exercises
    -  Execute on both local and torch servers

3. [A3 README.md](a3/README.md "My A3 README.md file")
    -  Install Oracle login and create accessibility
    -  Create and populate tables
    -  Type codes and run queries to 25 questions for required reports


4. [A4 README.md](a4/README.md "My A4 README.md file")
    -  Install MS SQL, login and create accessibility
    -  Create order system for a home office supply company as per Business Rules
    -  Create and populate tables of database
    -  Type codes and run queries to 6 questions for required reports


5. [A5 README.md](a5/README.md "My A5 README.md file")
    -  Update order system for a home office supply company as per Business Rules
    -  Create and populate five additional tables of database
    -  Amplify attibutes of region in the database
    -  Type codes and run queries to 6 questions for stored procedure and trigger reports


6. [P1 README.md](p1/README.md "My P1 README.md file")
    -  Create and populate tables according to the Business Rules of a local municipality. 
    -  Type codes and run queries for SQL statement questions.
    -  Provide a screenshot of the ERD for the municipality database.
    -  Provide a BitBucket repos link.


7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Install Mongo DB
    - Upload "restaurants" file in json
    - Write commands for DB searches
    - Provide screenshots of code for queries