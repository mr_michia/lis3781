/*
Notes: Tables *must* include the following constraints and defaults:
* per_ssn: must be unique (see indexes/keys), and SHA2_512 hashed
* per_gender: m or f
* per_type: c or s
Example: ([per_zip] like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
* phone num: require entries in phone column to be 10 digits
Example: ([phn_num] like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
* phone type: home, cell, work, fax
Example: ([phy_type]='f' OR [phy_type]='w' OR [phy_type]='c' OR [phy_type]='h')
* *all* byneruc vakyes: >= 0
Example: ([srp_yr_sales_goal] >= (0))

3. FK: Must require ON DELETE CASCADE, ON UPDATE CASCADE

Question: why use dbo.?
Answer:
From my research, even if the SQL code doesn't have to use the fully qualified name (e.g., dbo.customer),
evidently, there is a slight performance gain in doing so, because the optimizer doesn't have to look up the schema. 
And, it is considered a best practice.
http://www.sqlteam.com/article/understanding-the-difference-between-owners-and-schemas-in-sql-server
*/

-- (not the same, but) similar to SHOW WARNINGS;
set ANSI_WARNINGS ON;
GO

-- avoids error that user kept db connection open
use master;
GO

-- drop existing database if it exists (use *your* username)
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'agm18r')
DROP DATABASE agm18r;
GO

-- create database if not exists (use *your* username)
IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'agm18r')
CREATE DATABASE agm18r;
GO

USE agm18r;
GO

-- drop table if exists
-- N = subsequent string may be in Unicode (makes it portable to use with Unicode characters)
-- U = only look for objects with this name that are tables
-- *be sure* to use dbo. BEFORE *all* table references

-- ----------------------------------------------------------------------
-- TABLE: person
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.person',N'U') IS NOT NULL
DROP TABLE dbo.person;
GO

CREATE TABLE dbo.person
(
    per_id SMALLINT NOT NULL IDENTITY(1,1),
    per_ssn BINARY(64) NULL,
    per_salt BINARY(64) NULL,
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_gender CHAR(1) NOT NULL CHECK(per_gender IN ('m', 'f')),
    per_dob DATE NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL DEFAULT 'FL',
    per_zip INT NOT NULL CHECK (per_zip LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email VARCHAR(100) NULL,
    per_type CHAR(1) NOT NULL CHECK (per_type IN ('c', 's')),
    per_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

-- make sure SSNs and State IDs are unique
    CONSTRAINT ux_per_ssn UNIQUE NONCLUSTERED (per_ssn ASC)
);
GO

-- ----------------------------------------------------------------------
-- TABLE: phone
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;
GO

CREATE TABLE dbo.phone
(
    
    phn_id SMALLINT NOT NULL IDENTITY (1,1),
    per_id SMALLINT NOT NULL,
    phn_num BIGINT NOT NULL CHECK (phn_num LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type CHAR(1) NOT NULL CHECK (phn_type IN ('h', 'c', 'w', 'f')),
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    CONSTRAINT fk_phone_person
      FOREIGN KEY (per_id)
      REFERENCES dbo.person (per_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ----------------------------------------------------------------------
-- TABLE: customer
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.customer', N'U') IS NOT NULL
DROP TABLE dbo.customer;
GO

CREATE TABLE dbo.customer
(
    per_id SMALLINT NOT NULL,
    cus_balance DECIMAL(7,2) NOT NULL CHECK (cus_balance >=0),
    cus_total_sales DECIMAL(7,2) NOT NULL CHECK (cus_total_sales >= 0),
    cus_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_customer_person
      FOREIGN KEY (per_id)
      REFERENCES dbo.person (per_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ----------------------------------------------------------------------
-- TABLE: slsrep
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.slsrep', N'U') IS NOT NULL
DROP TABLE dbo.slsrep;
GO

CREATE TABLE dbo.slsrep
(
    per_id SMALLINT NOT NULL,
    srp_yr_sales_goal DECIMAL(8,2) NOT NULL CHECK (srp_yr_sales_goal >= 0),
    srp_ytd_sales DECIMAL(8,2) NOT NULL CHECK (srp_ytd_sales >= 0),
    srp_ytd_comm DECIMAL(7,2) NOT NULL CHECK (srp_ytd_comm >= 0),
    srp_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_slsrep_person
      FOREIGN KEY (per_id)
      REFERENCES dbo.person (per_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ----------------------------------------------------------------------
-- TABLE: srp_hist
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.srp_hist', N'U') IS NOT NULL
DROP TABLE dbo.srp_hist;
GO

CREATE TABLE dbo.srp_hist
(
  sht_id SMALLINT NOT NULL IDENTITY(1,1),
  per_id SMALLINT NOT NULL,
  sht_type CHAR(1) NOT NULL CHECK (sht_type IN ('i', 'u', 'd')),
  sht_modified DATETIME NOT NULL,
  sht_modifier VARCHAR(45) NOT NULL DEFAULT SYSTEM_USER,
  sht_date DATE NOT NULL DEFAULT GETDATE(),
  sht_yr_sales_goal DECIMAL(8,2) NOT NULL CHECK (sht_yr_sales_goal >= 0),
  sht_yr_total_sales DECIMAL(8,2) NOT NULL CHECK (sht_yr_total_sales >= 0),
  sht_yr_total_comm DECIMAL(7,2) NOT NULL CHECK (sht_yr_total_comm >= 0),
  sht_notes VARCHAR(45) NULL,
  PRIMARY KEY (sht_id),

  CONSTRAINT fk_srp_hist_slsrep
    FOREIGN KEY (per_id)
    REFERENCES dbo.slsrep (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);


-- ----------------------------------------------------------------------
-- TABLE: contact
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.contact', N'U') IS NOT NULL
DROP TABLE dbo.contact;
GO

CREATE TABLE dbo.contact
(
    cnt_id INT NOT NULL IDENTITY(1,1),
    per_cid SMALLINT NOT NULL,
    per_sid SMALLINT NOT NULL,
    cnt_date DATETIME NOT NULL,
    cnt_notes VARCHAR(255) NULL,
    PRIMARY KEY (cnt_id),

    CONSTRAINT fk_contact_customer
      FOREIGN KEY (per_cid)
      REFERENCES dbo.customer (per_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,

    CONSTRAINT fk_contact_slsrep
      FOREIGN KEY (per_sid)
      REFERENCES dbo.slsrep (per_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

/* 
NOTE: Cannot have CASCADE paths on both fks in CONTACT, because records in CONTACT table are linked with CUSTOMER records,
*and* records in CONTACT table are linked with SLSREP records. Even if records in CONTACT table never belonged to both CUSTOMER and SLSREP,
it would be impossible to make CONTACT table's records cascade delete for both CUSTOMER and SLSREP tables,
because there are multiple cascading paths from PERSONT to CONTACT table (one via CUSTOMER and one via SLSREP). Cannot do *BOTH* at the same time!
Hence, MS SQL Server will generate an error when attempting to create both cascading fks.
*/

-- ----------------------------------------------------------------------
-- TABLE: [order]
-- must use delimiter [] for reserved words (e.g., order)
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.[order]', N'U') IS NOT NULL
DROP TABLE dbo.[order];
GO

CREATE TABLE dbo.[order]
(
    ord_id INT NOT NULL IDENTITY(1,1),
    cnt_id INT NOT NULL,
    ord_placed_date DATETIME NOT NULL,
    ord_filled_date DATETIME NULL,
    ord_notes VARCHAR(255) NULL,
    PRIMARY KEY (ord_id),

    CONSTRAINT fk_order_contact
      FOREIGN KEY (cnt_id)
      REFERENCES dbo.contact (cnt_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ----------------------------------------------------------------------
-- TABLE: store
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store
(
    str_id SMALLINT NOT NULL IDENTITY(1,1),
    str_name VARCHAR(45) NOT NULL,
    str_street VARCHAR(30) NOT NULL,
    str_city VARCHAR(30) NOT NULL,
    str_state CHAR(2) NOT NULL DEFAULT 'FL',
    str_zip INT NOT NULL CHECK (str_zip LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone BIGINT NOT NULL CHECK (str_phone LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email VARCHAR(100) NOT NULL,
    str_url VARCHAR(100) NOT NULL,
    str_notes VARCHAR(255) NULL,
    PRIMARY KEY (str_id)
);

-- ----------------------------------------------------------------------
-- TABLE: invoice
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.invoice', N'U') IS NOT NULL
DROP TABLE dbo.invoice;
GO

CREATE TABLE dbo.invoice
(
    inv_id INT NOT NULL IDENTITY(1,1),
    ord_id INT NOT NULL,
    str_id SMALLINT NOT NULL,
    inv_date DATETIME NOT NULL,
    inv_total DECIMAL(8,2) NOT NULL CHECK (inv_total >= 0),
    inv_paid BIT NOT NULL,
    inv_notes VARCHAR(255) NULL,
    PRIMARY KEY (inv_id),

-- create 1:1 relationship with order by making ord_id unique
    CONSTRAINT ux_ord_id UNIQUE NONCLUSTERED (ord_id ASC),

    CONSTRAINT fk_invoice_order
      FOREIGN KEY (ord_id)
      REFERENCES dbo.[order] (ord_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,

    CONSTRAINT fk_invoice_store
      FOREIGN KEY (str_id)
      REFERENCES dbo.store (str_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ----------------------------------------------------------------------
-- TABLE: payment
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.payment', N'U') IS NOT NULL
DROP TABLE dbo.payment;
GO

CREATE TABLE dbo.payment
(
    pay_id INT NOT NULL IDENTITY(1,1),
    inv_id INT NOT NULL,
    pay_date DATETIME NOT NULL,
    pay_amt DECIMAL(7,2) NOT NULL CHECK (pay_amt >= 0),
    pay_notes VARCHAR(255) NULL,
    PRIMARY KEY (pay_id),

    CONSTRAINT fk_payment_invoice
      FOREIGN KEY (inv_id)
      REFERENCES dbo.invoice (inv_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ----------------------------------------------------------------------
-- TABLE: vendor
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.vendor', N'U') IS NOT NULL
DROP TABLE dbo.vendor;
GO

CREATE TABLE dbo.vendor
(
    ven_id SMALLINT NOT NULL IDENTITY(1,1),
    ven_name VARCHAR(45) NOT NULL,
    ven_street VARCHAR(30) NOT NULL,
    ven_city VARCHAR(30) NOT NULL,
    ven_state CHAR(2) NOT NULL DEFAULT 'FL',
    ven_zip INT NOT NULL CHECK (ven_zip LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone BIGINT NOT NULL CHECK (ven_phone LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email VARCHAR(100) NULL,
    ven_url VARCHAR(100) NULL,
    ven_notes VARCHAR(255) NULL,
    PRIMARY KEY (ven_id)
);

-- ----------------------------------------------------------------------
-- TABLE: product
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.product', N'U') IS NOT NULL
DROP TABLE dbo.product;
GO

CREATE TABLE dbo.product
(
    pro_id SMALLINT NOT NULL IDENTITY(1,1),
    ven_id SMALLINT NOT NULL,
    pro_name VARCHAR(30) NOT NULL,
    pro_descript VARCHAR(45) NULL,
    pro_weight FLOAT NOT NULL CHECK (pro_weight >= 0),
    pro_qoh SMALLINT NOT NULL CHECK (pro_qoh >= 0),
    pro_cost DECIMAL(7,2) NOT NULL CHECK (pro_cost >= 0),
    pro_price DECIMAL(7,2) NOT NULL CHECK (pro_price >= 0),
    pro_discount DECIMAL(3,0) NULL,
    pro_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id),

    CONSTRAINT fk_product_vendor
      FOREIGN KEY (ven_id)
      REFERENCES dbo.vendor (ven_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ----------------------------------------------------------------------
-- TABLE: product_hist
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.product_hist', N'U') IS NOT NULL
DROP TABLE dbo.product_hist;
GO

CREATE TABLE dbo.product_hist
(
    pht_id INT NOT NULL IDENTITY(1,1),
    pro_id SMALLINT NOT NULL,
    pht_date DATETIME NOT NULL,
    pht_cost DECIMAL(7,2) NOT NULL CHECK (pht_cost >= 0),
    pht_price DECIMAL(7,2) NOT NULL CHECK (pht_price >= 0),
    pht_discount DECIMAL(3,0) NULL,
    pht_notes VARCHAR(255) NULL,
    PRIMARY KEY (pht_id),

    CONSTRAINT fk_product_hist_product
      FOREIGN KEY (pro_id)
      REFERENCES dbo.product (pro_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

-- ----------------------------------------------------------------------
-- TABLE: order_line
-- ----------------------------------------------------------------------
IF OBJECT_ID (N'dbo.order_line', N'U') IS NOT NULL
DROP TABLE dbo.order_line;
GO

CREATE TABLE dbo.order_line
(
    oln_id INT NOT NULL IDENTITY(1,1),
    ord_id INT NOT NULL,
    pro_id SMALLINT NOT NULL,
    oln_qty SMALLINT NOT NULL CHECK (oln_qty >= 0),
    oln_price DECIMAL(7,2) NOT NULL CHECK (oln_price >= 0),
    oln_notes VARCHAR(255) NULL,
    PRIMARY KEY (oln_id),

    CONSTRAINT fk_order_line_order
      FOREIGN KEY (ord_id)
      REFERENCES dbo.[order] (ord_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,

    CONSTRAINT fk_order_line_product
      FOREIGN KEY (pro_id)
      REFERENCES dbo.product (pro_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

SELECT * FROM information_schema.tables;

-- converts to binary
SELECT HASHBYTES('SHA2_512', 'test');

-- length 64 bytes
SELECT LEN(HASHBYTES('SHA2_512', 'test'));


-- ----------------------------------------------------------------------
-- DATA FOR TABLE: person
-- ----------------------------------------------------------------------
INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1, null, 'Steve', 'Rogers', 'm', '1923-10-03', '437 Southern Dr', 'Rochester', 'NY', 324402222, 'srogerscomcast.net', 's', NULL),
(2, NULL, 'Bruce', 'Wayne', 'm', '1968-03-20', '1007 Mountain Dr', 'Gotham', 'NY', 983208440, 'bwayne@knology.net', 's', NULL),
(3, NULL, 'Peter', 'Parker', 'm', '1988-09-12', '20 Ingram St', 'New York', 'NY', 102862341, 'pparker@msn.com','s', NULL),
(4, NULL, 'Jane', 'Thompson', 'f', '1978-05-08', '13563 Ocean View Dr', 'Seattle', 'WA', 132084409, 'jthompson@gmail.com', 's', NULL),
(5, NULL, 'Debra', 'Steele', 'f', '1994-07-19', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', 's', NULL),
(6, NULL, 'Tony', 'Stark', 'm', '1972-05-04', '332 Palm Ave', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', 'c', NULL),
(7, NULL, 'Hank', 'Pymi', 'm', '1980-08-28', '2355 Brown St', 'Cleveland', 'OH', 822348890, 'hpym@aol.com', 'c', NULL),
(8, NULL, 'Bob', 'Best', 'm', '1992-02-10', '4902 Avendale Ave', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', 'c', NULL),
(9, NULL, 'Sandra', 'Smith', 'f', '1990-01-26', '87912 Lawrence Ave', 'Atlanta', 'GA', 672348890, 'sdole@gmail.com', 'c', NULL),
(10, NULL, 'Ben', 'Avery', 'm', '1983-12-24', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', 'c', NULL),
(11, NULL, 'Arthur', 'Curry', 'm', '1975-12-15', '3304 Euclid Ave', 'Miami', 'FL', 342219932, 'accury@gmail.com', 'c', NULL),
(12, NULL, 'Diana', 'Price', 'f', '1980-08-22', '944 Green St', 'Las Vegas', 'NV', 332048823, 'dprice@symaptico.com', 'c', NULL),
(13, NULL, 'Adam', 'Smith', 'm', '1995-01-31', '98435 Valencia Dr', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', 'c', NULL),
(14, NULL, 'Judy', 'Sleen', 'f', '1970-03-22', '56343 Rover Ct', 'Billings', 'MT', 672048823, 'jsleen@symaptico.com', 'c', NULL),
(15, NULL, 'Bill', 'Neiderheim', 'm', '1982-06-13', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', 'c', NULL);


SELECT * FROM dbo.person;

-- ----------------------------------------------------------------------
-- DATA FOR TABLE: slsrep
-- ----------------------------------------------------------------------
INSERT INTO dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100, 60000, 1800, NULL),
(2, 200, 35000, 3500, NULL),
(3, 300, 84000, 9650, 'Great Salesperson!'),
(4, 400, 87000, 15300, NULL),
(5, 500, 43000, 8750, NULL);

SELECT * FROM dbo.slsrep;



-- ----------------------------------------------------------------------
-- DATA FOR TABLE: customer
-- ----------------------------------------------------------------------
INSERT INTO dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES
(6, 120, 14789, NULL),
(7, 98.46, 234.92, NULL),
(8, 0, 4578, 'Customer always pays on time.'),
(9, 981.73, 1672.38, 'High balance.'),
(10, 541.23, 782.57, NULL),
(11, 251.02, 13782.96, 'Good customer.'),
(12, 582.67, 963.12, 'Previously paid in full.'),
(13, 121.67, 1057.45, 'Recent customer'),
(14, 765.43, 6789.42, 'Buys bulk quantities.'),
(15, 304.39, 456.81, 'Has not purchased recently');

SELECT * FROM dbo.customer;

-- ----------------------------------------------------------------------
-- DATA FOR TABLE: contact
-- ----------------------------------------------------------------------
INSERT INTO dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
VALUES
(1, 6, '1999-01-01', NULL),
(2, 6, '2001-09-29', NULL),
(3, 7, '2002-08-15', NULL),
(2, 7, '2002-09-01', NULL),
(4, 7, '2004-01-05', NULL),
(5, 8, '2004-02-28', NULL),
(4, 8, '2004-03-03', NULL),
(1, 9, '2004-04-07', NULL),
(5, 9, '2004-07-29', NULL),
(3, 11, '2005-05-02', NULL),
(4, 13, '2005-06-14', NULL),
(2, 15, '2005-07-02', NULL);

SELECT * FROM dbo.contact;


-- ----------------------------------------------------------------------
-- DATA FOR TABLE: phone
-- ----------------------------------------------------------------------
INSERT INTO dbo.phone
(per_id, phn_num, phn_type, phn_notes)
VALUES
(2, 8505551234, 'c', 'Never Answers'),
(5, 8505551235, 'w', 'Never shuts up'),
(8, 8505551236, 'h', 'Too much information'),
(10, 8505551237, 'f', 'B-O-R-I-N-G!!'),
(14, 8505551238, 'c', 'Best customer!');

SELECT * FROM dbo.phone;


-- ----------------------------------------------------------------------
-- DATA FOR TABLE: order
-- ----------------------------------------------------------------------
INSERT INTO dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES
(1, '2010-11-23', '2010-12-24', NULL),
(2, '2005-03-19', '2005-07-28', NULL),
(3, '2011-07-01', '2011-07-06', NULL),
(4, '2009-12-24', '2010-01-05', NULL),
(5, '2008-09-21', '2008-11-26', NULL),
(6, '2009-04-17', '2009-04-30', NULL),
(7, '2010-05-31', '2010-06-07', NULL),
(8, '2007-09-02', '2007-09-16', NULL),
(9, '2011-12-08', '2011-12-23', NULL),
(10, '2012-02-29', '2012-05-02', NULL);

SELECT * FROM dbo.[order];

-- ----------------------------------------------------------------------
-- DATA FOR TABLE: store
-- ----------------------------------------------------------------------
INSERT INTO dbo.store
(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
('Walgreens', '14567 Walnut Ln','Aspen', 'IL', '475315690', '3127658127', 'info@walgreens.com', 'http//www.walgreens.com', NULL),
('CVS', '572 Casper Rd','Chicago', 'IL', '505231519', '3128926534', 'help@cvs.com', 'http//www.cvs.com', 'Rumor of merger.'),
('Lowes', '81309 Catapult Ave','Clover', 'WA', '802345671', '9017653421', 'sales@lowes.com', 'http//www.lowes.com', NULL),
('Walmart', '14567 Walnut Ln','St. Louis', 'FL', '387563628', '8722718923', 'info@walmart.com', 'http//www.walmart.com', NULL),
('Dollar General', '47583 Davidson Rd','Detoit', 'MI', '482983456', '3137583492', 'ask@dollargeneral.com', 'http//www.dollargeneral.com', 'Recently sold property');

SELECT * FROM dbo.store;

-- ----------------------------------------------------------------------
-- DATA FOR TABLE: invoice
-- ----------------------------------------------------------------------
INSERT INTO dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES
(5, 1, '2001-05-03', 58.32, 0, NULL),
(4, 1, '2006-11-11', 100.59, 0, NULL),
(1, 1, '2010-09-16', 57.34, 0, NULL),
(3, 2, '2011-01-10', 99.32, 1, NULL),
(2, 3, '2008-06-24', 1109.67, 1, NULL),
(6, 4, '2009-04-20', 239.83, 0, NULL),
(7, 5, '2010-06-05', 537.29, 0, NULL),
(8, 2, '2007-09-09', 644.21, 1, NULL),
(9, 3, '2011-12-17', 934.12, 1, NULL),
(10, 4, '2012-03-18', 27.45, 0, NULL);

SELECT * FROM dbo.invoice;

-- ----------------------------------------------------------------------
-- DATA FOR TABLE: vendor
-- ----------------------------------------------------------------------
INSERT INTO dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES
('Sysco', '531 Dolphin Run', 'Orlando', 'FL', '344761234', '7641238543', 'sales@sysco.com', 'http://www.sysco.com', NULL),
('General Electric', '100 Happy Trails Dr', 'Boston', 'MA', '123456874', '2134569641', 'support@ge.com', 'http://www.ge.com', 'Very good turnaround'),
('Cisco', '300 Cisco Dr', 'Stanford', 'OR', '872315492', '7823456723', 'cisco@cisco.com', 'http://www.cisco.com', NULL),
('Goodyear', '100 Goodyear Dr', 'Gary', 'IN', '485321956', '5784218427', 'sales@goodyear.com', 'http://www.goodyear.com', 'Competing well with Firestone'),
('Snap-On', '42185 Magenta Ave', 'Lake Falls', 'ND', '387513649', '9197345632', 'support@snapon.com', 'http://www.snap-on.com', 'Good quality tools!');

SELECT * FROM dbo.vendor;

-- ----------------------------------------------------------------------
-- DATA FOR TABLE: product
-- ----------------------------------------------------------------------
INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES
(1, 'hammer', '', 2.5, 45, 4.99, 7.99, 30, 'Discounted only wen purchased with screwdriver set.'),
(2, 'screwdriver', '', 1.8, 120, 1.99, 3.49, NULL, NULL),
(4, 'pail', '16 gallon', 2.8, 48, .89, 7.99, 40, NULL),
(5, 'cooking oil', 'Peanut oil', 15, 19, 19.99, 28.99, NULL, 'gallons'),
(3, 'frying pan', '', 3.5, 178, 8.45, 13.99, 50, 'Currently 1/2 price sale');

SELECT * FROM dbo.product;


-- ----------------------------------------------------------------------
-- DATA FOR TABLE: order_line
-- ----------------------------------------------------------------------
INSERT INTO dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
VALUES
(1, 2, 10, 8.0, NULL),
(2, 3, 7, 9.88, NULL),
(3, 4, 3, 6.99, NULL),
(5, 1, 2, 12.76, NULL),
(4, 5, 13, 58.99, NULL);

SELECT * FROM dbo.order_line;

-- ----------------------------------------------------------------------
-- DATA FOR TABLE: payment
-- ----------------------------------------------------------------------
INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(5, '2008-07-01', 5.99, NULL),
(4, '2010-09-28', 4.99, NULL),
(1, '2008-07-23', 8.75, NULL),
(3, '2010-10-31', 19.55, NULL),
(2, '2011-03-29', 32.5, NULL),
(6, '2010-10-03', 20.00, NULL),
(8, '2008-08-09', 1000.00, NULL),
(9, '2009-01-10', 103.68, NULL),
(7, '2007-03-15', 25.00, NULL),
(10, '2007-05-12', 40.00, NULL),
(4, '2007-05-22', 9.33, NULL);

SELECT * FROM dbo.payment;


-- ----------------------------------------------------------------------
-- DATA FOR TABLE: product_hist
-- ----------------------------------------------------------------------
INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1, '2005-01-02 11:53:34', 4.99, 7.99, 30, 'Discounted only when purchased with scrwdriver set'),
(2, '2005-02-03 09:13:56', 1.99, 3.49, NULL, NULL),
(3, '2005-03-04 23:21:49', 3.89, 7.99, 40, NULL),
(4, '2006-05-06 18:09:04', 19.99, 28.99, NULL, 'gallons'),
(5, '2006-05-07 15:07:29', 8.45, 13.99, 50, 'Currently 1/2 price sale');

SELECT * FROM dbo.product_hist;


-- ----------------------------------------------------------------------
-- DATA FOR TABLE: srp_hist
-- ----------------------------------------------------------------------
INSERT INTO dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
VALUES
(1, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 110000, 11000, NULL),
(2, 'i', getDate(), SYSTEM_USER, getDate(), 150000, 175000, 17500, NULL),
(3, 'u', getDate(), SYSTEM_USER, getDate(), 200000, 185000, 18500, NULL),
(4, 'u', getDate(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 22000, NULL),
(5, 'i', getDate(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, NULL);

SELECT * FROM dbo.srp_hist;

-- get year only from srp_hist table
SELECT year (sht_date) FROM dbo.srp_hist;
GO
-- SYSTEM_USER presents you with the credentials used to run the query.
-- This is important to establish which permissions were active.
--   Pro: you can see with which permissions a query was executed.
--   Con: You don't know who originally created the connection.

-- ORIGINAL_LOGIN is giving you the user with which the connection was established.
--   Pro: you can see who created the connection.
--   Con: You don't know with which permissions the query was executed.

-- http://www.differencebetween.net/business/difference-between-purchase-order-and-invoice/


CREATE PROC dbo.CreatePersonSSN
AS
BEGIN

  DECLARE @salt BINARY(64);
  DECLARE @ran_num INT;
  DECLARE @ssn BINARY(64);
  DECLARE @x INT, @y INT;
  SET @X = 1;

-- Dynamically set loop ending value (total number of persons)
  SET @y=(SELECT COUNT(*) FROM dbo.person);

-- SELECT @y; -- display number of persons (only for testing)

     WHILE (@x <= @y)
     BEGIN

-- give each person a unique randomized salt, and hashed and salted randomized SSN.
-- NOTE: this demo is *only* for showing how to include salted and hashed randomized values for testing purposes!
-- function returns a cryptogrphic, randomly-generated hexadecimal number with length of specified number of bytes

     SET @salt=CRYPT_GEN_RANDOM(64);
     SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; 
     SET @ssn=HASHBYTES('SHA2_512', CONCAT(@salt, @ran_num));

     UPDATE dbo.person
     SET per_ssn=@ssn, per_salt=@salt
     WHERE per_id=@x;

     SET @x = @x +1;

     END;

END;
GO

EXEC dbo.CreatePersonSSN
-- can also use EXECUTE dbo.CreatePersonSSN
GO
     



-- ****************************************************** BEGIN REPORTS ********************************************************

-- list tables
SELECT * FROM [agm18r].information_schema.tables;
GO

-- metadata of tables
SELECT * FROM [agm18r].information_schema.columns;
GO
-- summary informaiton of object
-- sp_help 'object_name'

-- summary information of object dbo.srp_hist
sp_help 'dbo.srp_hist';
GO

-- 1)	Create a view that displays the sum of all paid invoice totals for each customer, sort by the largest invoice total sum appearing first.
--  (a comparison of paid and unpaid invoices)

use agm18r;
GO

-- a. snapshot of all invoices
SELECT * FROM dbo.invoice;

-- b. snapshot of all *paid* invoices (i.e., inv_paid != 0);
SELECT inv_id, inv_total as paid_invoice_total
FROM dbo.invoice
WHERE inv_paid !=0;

-- use single quotation mark to escape single quotation mark (otherwise, error), also, notice additional space for line break
PRINT '#1 Solution: create view (sum of each cusotmer''s *paid* invoices, in desc order):

';

-- drop view if exists
-- 1st arg is object name, 2nd arg is type (V=view)
IF OBJECT_ID (N'dbo.v_paid_invoice_total', N'V') IS NOT NULL
DROP VIEW dbo.v_paid_invoice_total;
GO

-- In MS SQL Server do *NOT* use ORDER BY clause in *VIEWS* (non-guaranteed behavior)
CREATE VIEW dbo.v_paid_invoice_total AS
  SELECT p.per_id, per_fname, per_lname, sum(inv_total) AS sum_total, FORMAT(sum(inv_total), 'C', 'en-us') AS paid_invoice_total
  FROM dbo.person p
    JOIN dbo.customer c ON p.per_id=c.per_id
    JOIN dbo.contact ct ON c.per_id=ct.per_cid
    JOIN dbo.[order] o ON ct.cnt_id=o.cnt_id
    JOIN dbo.invoice i ON o.order_id=i.ord_id
  WHERE inv_paid !=0
-- mustbe contained in group by, if not used in aggregate function
    GROUP BY p.per_id, per_fname, per_lname;
GO

-- display view results (order by should be used outside of view)
SELECT per_id, per_fname, per_lname, paid_invoice_total FROM dbo.v_paid_invoice_total ORDER BY sum_total DESC
GO

-- compare views to base tables
SELECT * FROM information_schema.tables;
GO

-- 2)	Create a stored procedure that displays all customers’ outstanding balances (unstored derived attribute based upon the difference of a customer's invoice 
--    total and their respective payments). List their invoice totals, what was paid, and the difference.

-- a. individual customer
SELECT p.per_id, per_fname, per_lname,
SUM(pay_amt) AS total_paid, (inv_total - sum(pay_amt)) invoice_diff
  FROM person p
    JOIN dbo.customer c ON p.per_id=c.per_id
    JOIN dbo.contact ct ON c.per_id=ct.per_cid
    JOIN dbo.[order] o ON ct.cnt_id=o.cnt_id
    JOIN dbo.invoice i ON o.ord_id=i.ord_id
    JOIN dbo.payment pt ON i.inv_id=pt.inv_id
-- must be contained in group by, if not used in aggregate function
  WHERE p.per_id=7
  GROUP BY p.per_id, per_fname, per_lname, inv_total;

PRINT '#2 Solution: create procedure (displays all customer'' outstanding balances):

';

-- 1st arg is object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.sp_all_customers_outstanding_balances', N'P') IS NOT NULL
DROP PROC dbo.sp_all_customers_outstanding_balances
GO

-- In MS SQL Server: *can* use ORDER BY clsue in stored procedures, through, *not* views
CREATE PROC dbo.sp_all_customers_outstanding_balances AS
BEGIN
  SELECT p.per_id, per_fname, per_lname,
  sum(pay_amt) AS total_paid, (inv_total - sum(pay_amt)) invoice_diff
  FROM person p
    JOIN dbo.customer c on p.per_id=c.per_id
    JOIN dbo.contact ct ON c.per_id=ct.per_cid
    JOIN dbo.[order] o ON ct.cnt_id=o.cnt_id
    JOIN dbo.invoice i ON o.ord_id=i.ord_id
    JOIN dbo.payment pt ON i.inv_id=pt.inv_id
-- must be contained in group by, if not used in aggregate function
  GROUP BY p.per_id, per_fname, per_lname, inv_total
  ORDER BY invoice_diff DESC;
END
GO

-- call stored procedure
EXEC dbo.sp_all_customers_outstanding_balances;

-- list all procedures (e.g., stored procedures or functions) for database
SELECT * FROM agm18r.information_schema.routines
  WHERE routine_type = 'PROCEDURE';
GO

-- Display definition of trigger, stored procedure or view
sp_helptext 'dbo.sp_all_customers_outstanding_balances'
GO

-- 3)	Create a stored procedure that populates the sales rep history table w/sales reps’ data when called. 

-- NOTE: BOTH tables have existing data
-- Demonstration illustrates how to initially populate a table w/ antoher table's data, while adding dynamically generated data.

PRINT '#3 Solution: Create stored procedure to populate history table wit sales reps'' data when called

';

-- 1st arg is object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.sp_populate_srp_hist_table', N'P') IS NOT NULL
DROP PROC dbo.sp_populate_srp_hist_table
GO

CREATE PROC dbo.sp_populate_srp_hist_table AS
BEGIN
  INSERT INTO dbo.srp_hist
  (per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
-- Mix dynamically generated data, with original sales reps' data
  SELECT per_id, 'i', getDate(), SYSTEM_USER, getDate(), srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes
  FROM dbo.slsrep;
END
GO

PRINT 'List table data before call:

';

-- Purposefully deleting original data to simulate initially populating a "log" or history table
DELETE FROM dbo.srp_hist;

-- Call stored procedure (populating srp_hist table with slsrep table data)
EXEC dbo.sp_populate_srp_hist_table;

PRINT 'List table data after call:

';
SELECT * FROM dbo.slsrep;
SELECT * FROM dbo.srp_hist;

-- list all procedures (e.g., stored procedures or functions) for database
SELECT * FROM agm18r.information_schema.routines
  WHERE routine_type = 'PROCEDURE';
GO


-- 4)	Create a trigger that automatically adds a record to the sales reps’ history table for every record added to the sales rep table. 

PRINT '#4 Solution: Create a trigger that automatically adds a record to the sales reps'' history table for every record added to the sales rep table

';

/*
NOTE: when using MS SQL Server triggers, there are two system tables created "Inserted" and "Deleted."
Inserted: contains new rows for insert and update operations.
Deleted: contains original rows for update and delete operations.
*/

-- 1st arg is object name, 2nd arg is type (TR = Trigger)
IF OBJECT_ID(N'dbo.trg_sales_history_insert', N'TR') IS NOT NULL
DROP TRIGGER dbo.trg_sales_history_insert
GO

CREATE TRIGGER dbo.trg_sales_history_insert
ON dbo.slsrep
AFTER INSERT AS
BEGIN

-- declare variables
  DECLARE
  @per_id_v SMALLINT,
  @sht_type_v CHAR(1),
  @sht_modified_v DATE,
  @sht_modifier_v VARCHAR(45),
  @sht_date_v DATE,
  @sht_yr_sales_goal_v DECIMAL(8,2),
  @sht_yr_total_sales_v DECIMAL(8,2),
  @sht_yr_total_comm_v DECIMAL(7,2),
  @sht_notes_v VARCHAR(255);

  SELECT
  @per_id_v = per_id,
  @sht_type_v = 'i',
  @sht_modified_v = getDate(),
  @sht_modifier_v = SYSTEM_USER,
  @sht_date_v = getDate(),
  @sht_yr_sales_goal_v = srp_yr_sales_goal, 
  @sht_yr_total_sales_v = srp_ytd_sales,
  @sht_yr_total_comm_v = srp_ytd_comm,
  @sht_notes_v = srp_notes
  FROM INSERTED;

  INSERT INTO dbo.srp_hist
  (per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
  VALUES
  (@per_id_v, @sht_type_v, @sht_modified_v, @sht_modifier_v, @sht_date_v, @sht_yr_sales_goal_v, @sht_yr_total_sales_v, @sht_yr_total_comm_v, @sht_notes_v)
END
GO

PRINT 'List table data before trigger fires:

';

SELECT * FROM slsrep;
SELECT * FROM srp_hist;

-- Fire trigger
INSERT INTO dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(6, 98000, 43000, 8750, 'per_id values 1-5 already used');

PRINT 'List table data after trigger fires:

';
SELECT * FROM slsrep;
SELECT * FROM srp_hist;


-- 5)	Create a trigger that automatically adds a record to the product history table for every record added to the product table. 

PRINT '#5 Solution: Create a trigger that automatically adds a record to the product history table for every record added to the product table: 

';

IF OBJECT_ID(N'dbo.trg_product_history_insert', N'TR') IS NOT NULL
DROP TRIGGER dbo.trg_product_history_insert
GO

CREATE TRIGGER dbo.trg_product_history_insert
ON dbo.product
AFTER INSERT AS
BEGIN
  DECLARE
  @pro_id_v SMALLINT,
--  @pht_type_v CHAR(1), -- insert, update, delete
  @pht_modified_v DATE, -- when recorded
--  @pht_modifier_v VARCHAR(45), -- who made the change
  @pht_cost_v DECIMAL(7,2),
  @pht_price_v DECIMAL(7,2),
  @pht_discount_v DECIMAL(3,0),
  @pht_notes_v VARCHAR(255);

  SELECT
  @pro_id_v = pro_id,
  @pht_modified_v = getDate(),
  @pht_cost_v = pro_cost,
  @pht_price_v = pro_price,
  @pht_discount_v = pro_discount,
  @pht_notes_v = pro_notes
  FROM INSERTED;

  INSERT INTO dbo.product_hist
  (pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
   VALUES
  (@pro_id_v, @pht_modified_v, @pht_cost_v, @pht_price_v, @pht_discount_v, @pht_notes_v);
  
END
GO

PRINT 'List table data BEFORE trigger fires:

';

SELECT * FROM product;
SELECT * FROM product_hist;

-- Fire trigger
INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES
(3, 'desk lamp', 'small desk lamp with led lights', 3.6, 14, 5.98, 11.99, 15, 'No discount after sale.');


PRINT 'List table data AFTER trigger fires:

';

SELECT * FROM product;
SELECT * FROM product_hist;


-- Create a stored procedure that updates sales reps’ yearly_sales_goal in the slsrep table, based upon 8% more 
-- than their previous year’s total sales (sht_yr_total_sales), name it sp_annual_salesrep_sales_goal.

PRINT '**Extra Credit*** Solution: Stored procedure updates sales reps'' yearly_sales_goal in the slsrep table, basedbased upon 8% more than their previous year''s total sales (sht_yr_total_sales), name it sp_annual_salesrep_sales_goal

';

-- 1st arg is object name, 2nd arg is type (P = Procedure)
IF OBJECT_ID(N'dbo.sp_annual_salesrep_sales_goal', N'P') IS NOT NULL
DROP PROC dbo.sp_annual_salesrep_sales_goal
GO

CREATE PROC dbo.sp_annual_salesrep_sales_goal AS
BEGIN
-- update is based upon 8% of each sales rep's previous year's individual total sales (Notes: see sht_yr_total_sales in srp_hist table)
  UPDATE slsrep
  SET srp_yr_sales_goal = sht_yr_total_sales * 1.08
  FROM slsrep AS sr
    JOIN srp_hist AS sh
    ON sr.per_id = sh.per_id
-- NOTE: since all data is recent, use max() function for testing
    WHERE sht_date=(SELECT MAX(sht_date) FROM srp_hist);
END
GO

PRINT 'List table data BEFORE call:

';

SELECT * FROM dbo.slsrep;
SELECT * FROM dbo.srp_hist;

-- call stored procedure
EXEC dbo.sp_annual_salesrep_sales_goal

PRINT 'List table data AFTER call:

';

SELECT * FROM dbo.slsrep;
SELECT * FROM dbo.srp_hist;


-- LIst all procedures (e.g., stored procedures or functions) for database
SELECT * FROM agm18r.information_schema.routines
WHERE routine_type = 'PROCEDURE';
GO