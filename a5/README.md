> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database

## Adam Michia


### Assignment A5 Requirements:

1. Update order system for a home office supply company as per Business Rules
2. Create and populate five additional tables of database
3. Amplify attibutes of region in the database
4. Type codes and run queries to 6 questions for stored procedure and trigger reports



## A5 Database Business Rules:


     Business Rules: 
     Expanding upon the high-volume home office supply company’s data tracking requirements, the CFO requests your services again to extend the data model’s functionality. The CFO has read about the capabilities of data warehousing analytics and business intelligence (BI), and is looking to develop a smaller data mart as a test platform. He is under pressure from the members of the company’s board of directors who want to review more detailed sales reports based upon the following measurements: 
          1.	Product 
          2.	Customer 
          3.	Sales representative 
          4.	Time (year, quarter, month, week, day, time) 
          5.	Location 

     Furthermore, the board members want location to be expanded to include the following characteristics of location: 
          1.	Region 
          2.	State 
          3.	City 
          4.	Store 



#### README.md file includes the following items:

* Screenshot of *your* ERD; 
* Screenshot: At least *one* required report (i.e., exercise below), and SQL code solution. 
* Bitbucket repo links: *Your* lis3781 Bitbucket repo link 



#### Assignment Screenshots:

*Screenshot#1 of A5 ERD*:

![LIS3781 ERD for Assignment 5 Screenshot](img/erd.png)

