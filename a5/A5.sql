--(not the same, but) similar to SHOW WARNINGS;
SET ANSI_WARNINGS ON;
GO
use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'agm18r')
DROP DATABASE agm18r;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'agm18r')
CREATE DATABASE agm18r;
GO

use agm18r;
GO


IF OBJECT_ID (N'dbo.applicant', N'U') IS NOT NULL
DROP TABLE dbo.applicant;
GO
CREATE TABLE dbo.applicant
(
app_id SMALLINT not null identity(1,1),
app_ssn int NOT NULL check (app_ssn > 0 and app_ssn <= 999999999),
app_state_id VARCHAR(45) NOT NULL,
app_fame VARCHAR(15) NOT NULL,
app_lname VARCHAR(30) NOT NULL,
app_street VARCHAR(30) NOT NULL,
app_city VARCHAR(30) NOT NULL,
app_state CHAR(2) NOT NULL DEFAULT 'FL',

app_zip char(9) NOT NULL check (app_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
app_email VARCHAR(100) NOT NULL,
app_dob DATE NOT NULL,
app_gender CHAR(1) NOT NULL CHECK (app_gender IN('m', 'f')),
app_bckgd_check CHAR(1) NOT NULL CHECK (app_bckgd_check IN('n', 'y')),
app_notes VARCHAR(255) NULL,
PRIMARY KEY (app_id),

CONSTRAINT ux_app_ssn unique nonclustered (app_ssn ASC),
CONSTRAINT ux_app_state_id unique nonclustered (app_state_id ASC)
);



IF OBJECT_ID (N'dbo.property', N'U') IS NOT NULL
DROP TABLE dbo.property;
CREATE TABLE dbo.property
(
prp_id SMALLINT NOT NULL identity(1,1),
prp_street VARCHAR(30) NOT NULL,
prp_city VARCHAR(30) NOT NULL,
prp_state CHAR (2) NOT NULL DEFAULT 'FL',


prp_zip char(9) NOT NULL check (prp_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
prp_type varchar(15) NOT NULL CHECK (prp_type IN('house', 'condo', 'townhouse', 'duplex', 'apt', 'mobile home', 'room')),
prp_rental_rate DECIMAL(7,2) NOT NULL CHECK (prp_rental_rate > 0),
prp_status CHAR(1) NOT NULL CHECK (prp_status IN('a','u')),
prp_notes VARCHAR(255) NULL,
PRIMARY KEY (prp_id)
);

IF OBJECT_ID (N'dbo.agreement', N'U') IS NOT NULL
DROP TABLE dbo.agreement;
CREATE TABLE dbo.agreement
(
agr_id SMALLINT NOT NULL identity(1,1),
prp_id SMALLINT NOT NULL,
app_id SMALLINT NOT NULL,
agr_signed DATE NOT NULL,
agr_start DATE NOT NULL,
agr_end DATE NOT NULL,
agr_amt DECIMAL(7,2) NOT NULL CHECK (agr_amt > 0),
agr_notes VARCHAR(255) NULL,
PRIMARY KEY (agr_id),

CONSTRAINT ux_prp_id_app_id_agr_signed unique nonclustered (prp_id ASC, app_id ASC, agr_signed DESC),
CONSTRAINT fk_agreement_property
FOREIGN KEY (prp_id)
REFERENCES dbo.property (prp_id)
ON DELETE CASCADE
ON UPDATE CASCADE,
CONSTRAINT fk_agreement_applicant
FOREIGN KEY (app_id)
REFERENCES dbo.applicant (app_id)
ON DELETE CASCADE
ON UPDATE CASCADE
);


IF OBJECT_ID (N'dbo.feature', N'U') IS NOT NULL
DROP TABLE dbo.feature;
CREATE TABLE dbo.feature
(
ftr_id TINYINT NOT NULL identity(1,1),
ftr_type VARCHAR(45) NOT NULL,
ftr_notes VARCHAR(255) NULL,
PRIMARY KEY (ftr_id)
);


IF OBJECT_ID (N'dbo.prop_feature', N'U') IS NOT NULL
DROP TABLE dbo.prop_feature;
CREATE TABLE dbo.prop_feature
(
pft_id SMALLINT NOT NULL identity(1,1),
prp_id SMALLINT NOT NULL,
ftr_id TINYINT NOT NULL,
pft_notes VARCHAR(255) NULL,
PRIMARY KEY (pft_id),
--make sure combination of prp_id and ftr_id is unique
CONSTRAINT ux_prp_id_ftr_id unique nonclustered (prp_id ASC, ftr_id ASC),
CONSTRAINT fk_prop_feat_property
FOREIGN KEY (prp_id)
REFERENCES dbo.property (prp_id)
ON DELETE CASCADE
ON UPDATE CASCADE,
CONSTRAINT fk_prop_feat_feature
FOREIGN KEY (ftr_id)
REFERENCES dbo.feature (ftr_id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

IF OBJECT_ID (N'dbo.occupant', N'U') IS NOT NULL
DROP TABLE dbo.occupant;
CREATE TABLE dbo.occupant
(
ocp_id SMALLINT NOT NULL identity(1,1),
app_id SMALLINT NOT NULL,
ocp_ssn int NOT NULL check (ocp_ssn > 0 and ocp_ssn <= 999999999),
ocp_state_id VARCHAR(45) NULL,
ocp_fname VARCHAR(15) NOT NULL,
ocp_Iname VARCHAR(30) NOT NULL,
ocp_email VARCHAR(100) NULL,
OCP_dob DATE NOT NULL,
Ocp_gender CHAR(1) NOT NULL CHECK (ocp_gender IN('m', 'f')),
ocp_bckgd_check CHAR(1) NOT NULL CHECK (ocp_bckgd_check IN('n', 'y')),
Ocp_notes VARCHAR(45) NULL,
PRIMARY KEY (ocp_id),

CONSTRAINT ux_cp_ssn unique nonclustered (ocp_ssn ASC),
CONSTRAINT ux_ocp_state_id unique nonclustered (ocp_state_id ASC),
CONSTRAINT fk_occupant_applicant
FOREIGN KEY (app_id)
REFERENCES dbo.applicant (app_id)
ON DELETE CASCADE
ON UPDATE CASCADE
);




IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;
-- Note: an applicant *must* provide a phone number, but can have null values, if phone(s) belong(s) exclusively to their occupants
CREATE TABLE dbo.phone
(
phn_id SMALLINT NOT NULL identity(1,1),
app_id SMALLINT NULL, -- can be null, but *must* provide at least one number
ocp_id SMALLINT NULL,
-- OLD phone: phn_num bigint NOT NULL check (phn_num > 0 and phn_num <= 9999999999),

phn_num bigint NOT NULL check (phn_num like '[0-91[0-91[0-91[0-91[0-91[0-91[0-91[0-9150-9150-91'),
phn_type CHAR(1) NOT NULL CHECK (phn_type IN('c','h','w','f')),
phn_notes VARCHAR(255) NULL,
PRIMARY KEY (phn_id),

CONSTRAINT ux_app_id_phn_num unique nonclustered (app_id ASC, phn_num ASC),

CONSTRAINT ux_ocp_id_phn_num unique nonclustered (ocp_id ASC, phn_num ASC),
CONSTRAINT fk_phone_applicant
FOREIGN KEY (app_id)
REFERENCES dbo.applicant (app_id)
ON DELETE CASCADE
ON UPDATE CASCADE,


CONSTRAINT fk_phone_occupant
FOREIGN KEY (ocp_id)
REFERENCES dbo.occupant (ocp_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
);

IF OBJECT_ID (N'dbo.room_type', N'U') IS NOT NULL
DROP TABLE dbo.room_type;
CREATE TABLE dbo.room_type
(
rtp_id TINYINT NOT NULL identity(1,1),
rtp_name VARCHAR(45) NOT NULL,
rtp_notes VARCHAR(255) NULL,
PRIMARY KEY (rtp_id)
);




IF OBJECT_ID (N'dbo.room', N'U') IS NOT NULL
DROP TABLE bo.room;
CREATE TABLE dbo.room
(
rom_id SMALLINT NOT NULL identity(1,1),
prp_id SMALLINT NOT NULL,
rtp_id TINYINT NOT NULL,
rom_size VARCHAR(45) NOT NULL,
rom_notes VARCHAR(255) NULL,
PRIMARY KEY (rom_id),


CONSTRAINT ux_prp_id_rtp_id unique nonclustered (prp_id ASC, rtp_id ASC),
CONSTRAINT fk_room_property
FOREIGN KEY (prp_id)
REFERENCES dbo.property (prp_id)
ON DELETE CASCADE
ON UPDATE CASCADE,
CONSTRAINT fk_room_roomtype
FOREIGN KEY (rtp_id)
REFERENCES dbo.room_type (rtp_id)
ON DELETE CASCADE
ON UPDATE CASCADE
);


SELECT * FROM information_schema.tables;

EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"


INSERT INTO dbo.feature
(ftr_type, ftr_notes)
VALUES
('Central A/C', NULL),
('Pool', NULL),
('Close to school', NULL),
('Furnished', NULL),
('Cable', NULL),
('Washer/Dryer', NULL),
('Refrigerator', NULL),
('Microwave', NULL),
('Oven', NULL),
('1-car garage', NULL),
('2-car garage', NULL),
('Sprinkler system', NULL),
('Security', NULL),
('Wi-Fi', NULL),
('Storage', NULL),
('Fireplace', NULL);


INSERT INTO dbo.room_type
(rtp_name, rtp_notes)
VALUES
('Bed', NULL),
('Bath', NULL),
('Kitchen', NULL),
('Lanai', NULL),
('Dining', NULL),
('Living', NULL),
('Basement', NULL),
('Office', NULL);


INSERT INTO dbo.prop_feature
(prp_id, ftr_id, pft_notes)
VALUES
(1, 4, NULL),
(2, 5, NULL),
(3, 3, NULL),
(4, 2, NULL),
(5, 1, NULL),
(1, 1, NULL),
(1, 5, NULL);


INSERT INTO dbo.room
(prp_id, rtp_id, rom_size, rom_notes)
VALUES
(1,1, '1" � 10"', NULL),
(3,2, '20" x 15"', NULL),
(4,3, '8" x 8"', NULL),
(5,4, '50" � 50', NULL),
(2,5, '30" x 30"', NULL);

INSERT INTO dbo.property
(prp_street, prp_city, prp_state, prp_zip, prp_type, prp_rental_rate, prp_status, prp_notes)
VALUES
('5133 3rd Road', 'Lake Worth', 'FL', '334671234', 'house', 1800.00, 'u', NULL),
('92E Blah Way', 'Tallahassee', 'FL', '323011234', 'apt', 641.00, 'u', NULL),
('756 Diet Coke Lane', 'Panama City', 'FL', '342001234', 'condo', 2400.00, 'a', NULL),
('574 Doritos Circle', 'Jacksonville', 'F', '365231234', 'townhouse', 1942.09, 'a', NULL),
('2241 W. Pensacola Street', 'Tallahassee', 'FL', '323041234', 'apt', 610.00, 'u', NULL);

INSERT INTO dbo.applicant
(app_ssn, app_state_id, app_fname, app_lname, app_street, app_city, app_state, app_zip, app_email, app_dob, app_gender, app_bckgd_check, app_notes)
VALUES
('123456789', 'A12C34S56Q78', 'Carla', 'Vanderbilt', '5133 3rd Road', 'Lake Worth', 'FL', '334671234', 'csweeney@yahoo.com', '1961-11-26', 'F', 'Y', NULL),
('590123654', 'B123A456D789', 'Amanda', 'Lindell', '2241 W. Pensacola Street', 'Tallahassee', 'FL', '323041234', 'acc10c@my.fsu.edu', '1981-04-04', NULL),
('987456321', 'dfed66532sedd', 'David', 'Stephens', '1293 Banana Code Drive', 'Panama City', 'FL', '323081234', 'mjowett@comcast.net', '1965-05-15', 'M', 'n', NULL),
('323011234', 'landbeck@fsu.edu', '1969-07-25', 'M', 'Y', NULL);

INSERT INTO dbo.agreement
(prp_id, app_id, agr_signed, agr_start, agr_end, agr_amt, agr_notes)
VALUES
(3, 4, '2010-12-01', '2012-01-01', '2012-12-31', 1000.00, NULL),
(1, 1, '1983-01-01', '1983-01-01', '1987-12-31', 800.00, NULL),
(4, 2, '1999-12-3', '2000-01-01', '2004-12-31', 1200.00, NULL),
(5, 3, '1999-07-31', '1999-08-01', '2004-07-31', 750.00, NULL),
(2,5, '2011-01-01', '2011-01-01', '2013-12-31', 900.00, NULL);


INSERT INTO dbo.occupant
(app_id, ocp_ssn, ocp_state_id, ocp_fname, ocp_Iname, ocp_email, ocp_dob, ocp_gender, ocp_bckgd_check, ocp_notes)
VALUES
(1, '326532165', 'okd557ig4125', 'Bridget', 'Case-Sweeney', 'bcs10c@gmail.com', '1988-03-23', 'F', 'Y', NULL),
(1, '187452457', 'fikd654345', 'Brian', 'Sweeney', 'brian@sweeney.com', '1956-07-28', 'M', 'Y', NULL),
(2, '123456780', 'zalfj768591', 'Skittles', 'McGoobs', 'skittles@mcgoobs.com', '2011-01-01', 'F', 'n', NULL),
(2, '098123664', '396A0rierue', 'Jane', 'Doe', 'jdoe@aol.com','1988-03-05', 'M', 'n', NULL),
(5, '857694032', '324woruq', 'Bam', 'Bam', 'bbam@fstones.com', '2013-04-08', 'F', 'n', NULL);



INSERT INTO dbo.phone
(app_id, ocp_id, phn_num, phn_type, phn_notes)


VALUES
(1, NULL, '5615233044', 'H', NULL),
(2, NULL, '5616859976', 'F', NULL),
(5, 5, '8504569872', 'H', NULL),
(NULL, 1, '5613080898', 'C', 'occupant"s number only'),
(NULL, 1, '8182345678', 'W', NULL),
(NULL, 3, '3124567890', 'W', NULL),
(4, NULL, '2137890123', 'W', NULL),
(4, 2, '3049876543', 'W', NULL),
(3, 4, '3136543210', 'C', NULL),
(3, 1, '2123210987', 'C', NULL);

exec sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"

select * from dbo.feature;
select * from dbo.prop_feature;
select * from dbo.room_type;
select * from dbo.room;
select * from dbo.property;
select * from dbo.applicant;
select * from dbo.agreement;
select * from dbo.occupant;
select * from dbo.phone;