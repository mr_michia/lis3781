> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database

## Adam Michia


### Assignment A1 Requirements:

*Five Parts*

1. Distributed Version Control with Git and Bitbucket
2. MySQL Installation
3. Git commands
4. Entity Relationship Diagram
5. Bitbucket repo links:

    a. this assigment and

    b. the completed tutorial (bitbucketstationlocations)


## A1 Database Business Rules:

The human resource (HR) department of the ACME company wants to contract a database
modeler/designer to collect the following employee data for tax purposes: job description, length of
employment, benefits, number of dependents and their relationships, DOB of both the employee and any
respective dependents. In addition, employees’ histories must be tracked. Also, include the following
business rules:

     * Each employee may have one or more dependents.

     * Each employee has only one job.

     * Each job can be held by many employees.

     * Many employees may receive many benefits.

     * Many benefits may be selected by many employees.
 
In Addition:

     * Employee: SSN, DOB, start/end dates, salary;

     * Dependent: same information as their associated employee, type of relationship;

     * Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)

     * Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance)

     * Plan: type (single, spouse, family), cost, election date (plans must be unique)

     * Employee history: jobs, salaries, and benefit changes, as well as who made the change;

     * Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ);

     * *All* tables must include notes attribute.





#### README.md file includes the following items:

* Screenshots of A1 ERD
* Ex1. SQL Solution
* git commands w/ short descriptions



#### Git commands w/short descriptions:

1. git init - creates a new repository
2. git status - displays the state of the working directory and the staging area
3. git add - adds a change in the working directory to the staging area
4. git commit - saves changes in the local repository
5. git push - is used to upload local repository content to the remote depository
6. git pull - is used to update the local repository from the remote depository
7. git remote - is used to check what remote source you have or to add a new remote




#### Assignment Screenshots:

*Screenshot#1 of A1 ERD*:

![LIS3781 ERD for Assignment 1 Screenshot](img/erd.png)

*Screenshot#2 of A1 Ex1*:

![LIS3781 Select Statement in Workbench for Assignment 1 Screenshot](img/Ex1.png)



*Bitbucket Link to LIS3781 Assignment 1:*
[A1 This Assignment](https://bitbucket.org/mr_michia/lis3781/src/master/a1/ "Assignment 1")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mr_michia/lis3781/src/master/)


