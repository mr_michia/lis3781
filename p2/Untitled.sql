
use agm18r;

DROP TABLE IF EXISTS person;
CREATE TABLE IF NOT EXISTS person
(
per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
per_ssn BINARY(64) NULL,
per_salt binary(64) null,
per_fname VARCHAR(15) NOT NULL,
per_lname VARCHAR(30) NOT NULL,
per_street VARCHAR(30) NOT NULL,
per_city VARCHAR(30) NOT NULL,
per_state CHAR(2) NOT NULL,
per_zip CHAR(9) NOT NULL,
per_email VARCHAR(100) NOT NULL,
per_dob DATE NOT NULL,
per_type ENUM('a','c','j') NOT NULL,
per_notes VARCHAR(255) NULL,
PRIMARY KEY (per_id),
UNIQUE INDEX ux_per_ssn (per_ssn ASC)
)
ENGINE
InnoDB
DEFAULT CHARACTER SET=utf8MB4
COLLATE = utf8MB4_unicode_ci;
SHOW WARNINGS;

START TRANSACTION;
INSERT INTO person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve', 'Rogers', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'rogers@comcast.net', '1923-10-03', 'C', NULL),
(NULL, NULL, NULL, 'Bruce', 'Wayne', '1007 Mountain Drive', 'Gotham', 'NY', 003208440, 'bwayne@knology.net', '1968-03-20', 'C', NULL),
(NULL, NULL, NULL, 'Peter', 'Parker', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', '1988-09-12', 'C', NULL),
(NULL, NULL, NULL, 'Jane', 'Thompson', '13563 Ocean View Drive', 'Seattle', 'WA', 032084409, 'jthompson@gmail.com', '1978-05-08', 'C', NULL),
(NULL, NULL, NULL, 'Debra', 'Steele', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', '1994-07-19', 'C', NULL),
(NULL, NULL, NULL, 'Tony', 'Stark', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com','1972-05-04','a', NULL),
(NULL, NULL, NULL, 'Hank', 'Pymi', '2355 Brown Street', 'Cleveland','OH', 022348890, 'hpym@aol.com', '1980-08-28','a', NULL),
(NULL, NULL, NULL, 'Bob', 'Best', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', '1992-02-10', 'a', NULL),
(NULL, NULL, NULL, 'Sandra', 'Dole', '87912 Lawrence Ave', 'Atlanta','GA', 02348890, 'sdole@gmail.com', '1990-01-26', 'a', NULL),
(NULL, NULL, NULL, 'Ben', 'Avery', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', '1983-12-24', 'a', NULL),
(NULL, NULL, NULL, 'Arthur', 'Curry', '3304 Euclid Avenue', 'Miami', 'FL', 000219932, 'acurry@gmail.com', '1975-12-15', 'j', NULL),
(NULL, NULL, NULL, 'Diana', 'Price', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'price@symaptico.com', '1980-08-22', 'j', NULL),
(NULL, NULL, NULL, 'Adam', 'Jurris','98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', '1995-01-31', 'j', NULL),
(NULL, NULL, NULL, 'Judy', 'Sleen', '56343 Rover Ct.', 'Billings', 'MT', 672048823, 'jsleen@symaptico.com', '1970-03-22', 'j', NULL),
(NULL, NULL, NULL, 'Bill', 'Neiderheim', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', '1982-03-13', 'j', NULL);
COMMIT;

DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN
-- MySQL Variables: https://stackoverflow.com/questions/11754781/how-to-declare-a-variable-in-mysql
DECLARE x, y INT;
SET x = 1;
-- dynamically set loop ending value (total number of persons)
select count(*) into y from person;
-- select y: -- display number of persons (only for testing)
WHILE x <= Y DO
-- give each person a unique randomized salt, and hashed and salted randomized SSN.
-- Note: this demo is *only* for showing how to include salted and hashed randomized values for testing purposes!
SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user when looping
SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; -- random 9-digit SSN from 111111111 - 999999999, inclusive (see link below)
SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512));



update person
set per_ssn=@ssn, per_salt=@salt
where per_id=x;
SET X = x + 1;
END WHILE;
END$$
DELIMITER ;
call CreatePersonSSN();



select '4) Create a trigger that automatically adds a record to the judge history table for every record **added** to the judge table.' as '';
do sleep(5);
-- Note: technically, a judge could already be in the person table, yet not added to the judge table. Or, here, add a new person.
-- add 16th person (a judge), before adding person to judge table (salt and hash per ssn, and store unique salt, per salt)
select 'show person data *before* adding person record' as '';
select per_id, per_fname, per_lname from person;
do sleep(5);
-- give person a unique randomized salt, then hash and salt SSN
SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user
SET @num=000000000; -- Note: already provided random SSN from 111111111 - 999999999, inclusive above
SET @ssn=unhex(sha2(concat(@salt, @num), 512)); -- salt and hash person's SSN 000000000
INSERT INTO person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
values
(NULL, @ssn, @salt, 'Bobby', 'Sue', '123 Main St', 'Panama City Beach', 'FL', 324530221, 'bsue@fI.gov', '1962-05-16', 'j', 'new district judge');
select 'show person data *after* adding person record' as '';
select per _id, per_fame, per_Iname from person;
do sleep(5);

select * from person;
