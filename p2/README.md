> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database

## Adam Michia


### Assignment P2 Requirements:

*Four Parts*

1. Install Mongo DB
2. Upload "restaurants" file in json
3. Write commands for DB searches
4. Provide screenshots of code for queries


## P2 Data Requirments:

     *Import data into the collection: Place primer-dataset.json in bin directory!* 
     In system shell or command prompt (or Terminal), use mongoimport to insert the documents into the 
     restaurants collection in the test database. If the collection already exists in the test database, the 
     operation will drop the restaurants collection first.  
     • https://docs.mongodb.com/getting-started/shell/import-data/ 
     • https://docs.mongodb.com/getting-started/shell/import-data/#import-data-into-the-collection 




#### README.md file includes the following items:

* Screenshot of at least one MongoDB shell command(s), (e.g., show collections); 
* Screenshot: At least *one* required report (i.e., exercise below), and JSON code solution.  
* Bitbucket repo links: *Your* lis3781 Bitbucket repo link 




#### Assignment Screenshots:

*Screenshot of at least one MongoDB shell command*:

![LIS3781 Project 2.1 Screenshot](img/2.png)

*Screenshot#2 of JSON code solution*:

![LIS3781 Project 2.2 Screenshot](img/1.png)