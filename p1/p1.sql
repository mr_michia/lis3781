select 'drop, create, use database, create tables, display data:' as '';
do sleep(5);

Drop schema if exists agm18r;
create schema if not exists agm18r;
show warnings;
use agm18r;


-- Table person -------
drop table if exists person;
create table if not exists person(
	per_id smallint unsigned not null auto_increment,
    per_ssn binary(64) null,
    per_salt binary(64) null comment 'only for demo. DO NOT use salt name in prod',
    per_fname varchar(15) not null,
    per_lname varchar(30) not null,
    per_street varchar(30) not null,
    per_city varchar(30) not null,
    per_state char(2) not null,
    per_zip char(9) not null,
    per_email varchar(100) not null,
    per_dob date not null,
    per_type ENUM('a','c','j'),
    per_notes varchar(255) null,
    primary key (per_id),
    unique index ux_per_ssn (per_ssn ASC)
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table attorney -------
drop table if exists attorney;
create table if not exists attorney(
	per_id smallint unsigned not null,
    aty_start_date date not null,
    aty_end_date date default null,
    aty_hourly_rate decimal(5,2) not null,
    aty_years_in_practice tinyint not null,
    aty_notes varchar(255) null default null,
    primary key (per_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_attorney_person
    foreign key (per_id)
    references person (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table client -------
drop table if exists client;
create table if not exists client(
	per_id smallint unsigned not null,
    cli_notes varchar(255) null default null,
    primary key (per_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_client_person
    foreign key (per_id)
    references person (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table court -------
drop table if exists court;
create table if not exists court(
	crt_id tinyint unsigned not null auto_increment,
    crt_name varchar(45) not null,
    crt_street varchar(30) not null,
    crt_city varchar(30) not null,
    crt_state char(2) not null,
    crt_zip char(9) not null,
    crt_phone bigint not null,
    crt_email varchar(100) not null,
    crt_url varchar(100) not null,
    crt_notes varchar(255) null,
    primary key (crt_id)
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table judge -------
drop table if exists judge;
create table if not exists judge(
	per_id smallint unsigned not null,
    crt_id tinyint unsigned null default null,
    jud_salary decimal(8,2) not null,
    jud_years_in_practice tinyint unsigned not null,
    jud_notes varchar(255) null default null,
    primary key (per_id),
    
    index idx_per_id (per_id asc),
    index idx_crt_id (crt_id asc),
    
    constraint fk_judge_person
    foreign key (per_id)
    references person (per_id)
    on delete no action
    on update cascade,
    
    constraint fk_judge_court
    foreign key (crt_id)
    references court (crt_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table judge_hist -------
drop table if exists judge_hist;
create table if not exists judge_hist(
	jhs_id smallint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    jhs_crt_id tinyint null,
    jhs_date timestamp not null default current_timestamp,
    jhs_type enum('i','u','d') not null default 'i',
    jhs_salary decimal(8,2) not null,
    jhs_notes varchar(255) null default null,
    primary key (jhs_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_judge_hist_judge
    foreign key (per_id)
    references judge (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table case -------
drop table if exists `case`;
create table if not exists `case`(
	cse_id smallint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    cse_type varchar(45) not null,
    cse_description text not null,
    cse_start_date date not null,
    cse_end_date date null,
    cse_notes varchar(255) null,
    primary key (cse_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_court_case_judge
    foreign key (per_id)
    references judge (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table bar -------
drop table if exists bar;
create table if not exists bar(
	bar_id tinyint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    bar_name varchar(45) not null,    
    bar_notes varchar(255) null,
    primary key (bar_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_bar_attorney
    foreign key (per_id)
    references attorney (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table specialty -------
drop table if exists specialty;
create table if not exists specialty(
	spc_id tinyint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    spc_type varchar(45) not null,    
    spc_notes varchar(255) null,
    primary key (spc_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_specialty_attorney
    foreign key (per_id)
    references attorney (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table assignment -------
drop table if exists assignment;
create table if not exists assignment(
	asn_id smallint unsigned not null auto_increment,
    per_cid smallint unsigned not null,
    per_aid smallint unsigned not null,
    cse_id smallint unsigned not null,
    asn_notes varchar(255) null,
    primary key (asn_id),
    
    index idx_per_cid (per_cid asc),
    index idx_per_aid (per_aid asc),
    index idx_cse_id (cse_id asc),
    
    constraint fk_assign_case
    foreign key (cse_id)
    references `case` (cse_id)
    on delete no action
    on update cascade,
    
    constraint fk_assignment_client
    foreign key (per_cid)
    references client (per_id)
    on delete no action
    on update cascade,
    
    constraint fk_assignment_attorney
    foreign key (per_aid)
    references attorney (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

-- table phone  -------
drop table if exists phone;
create table if not exists phone(
	phn_id smallint unsigned not null auto_increment,
    per_id smallint unsigned not null,
    phn_num bigint unsigned not null,
    phn_type enum('h','c','w','f') not null comment 'home, cell, work, fax',
    phn_notes varchar(255) null,
    primary key (phn_id),
    
    index idx_per_id (per_id asc),
    
    constraint fk_phone_person
    foreign key (per_id)
    references person (per_id)
    on delete no action
    on update cascade
)
Engine = InnoDB
default character set=utf8mb4
collate = utf8mb4_0900_ai_ci;

show warnings;

#inserts for tables#

-- Person table -------
START TRANSACTION;
INSERT INTO person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve', 'Rogers', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'rogers@comcast.net', '1923-10-03', 'C', NULL),
(NULL, NULL, NULL, 'Bruce', 'Wayne', '1007 Mountain Drive', 'Gotham', 'NY', 003208440, 'bwayne@knology.net', '1968-03-20', 'C', NULL),
(NULL, NULL, NULL, 'Peter', 'Parker', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', '1988-09-12', 'C', NULL),
(NULL, NULL, NULL, 'Jane', 'Thompson', '13563 Ocean View Drive', 'Seattle', 'WA', 032084409, 'jthompson@gmail.com', '1978-05-08', 'C', NULL),
(NULL, NULL, NULL, 'Debra', 'Steele', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', '1994-07-19', 'C', NULL),
(NULL, NULL, NULL, 'Tony', 'Stark', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com','1972-05-04','a', NULL),
(NULL, NULL, NULL, 'Hank', 'Pymi', '2355 Brown Street', 'Cleveland','OH', 022348890, 'hpym@aol.com', '1980-08-28','a', NULL),
(NULL, NULL, NULL, 'Bob', 'Best', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', '1992-02-10', 'a', NULL),
(NULL, NULL, NULL, 'Sandra', 'Dole', '87912 Lawrence Ave', 'Atlanta','GA', 02348890, 'sdole@gmail.com', '1990-01-26', 'a', NULL),
(NULL, NULL, NULL, 'Ben', 'Avery', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', '1983-12-24', 'a', NULL),
(NULL, NULL, NULL, 'Arthur', 'Curry', '3304 Euclid Avenue', 'Miami', 'FL', 000219932, 'acurry@gmail.com', '1975-12-15', 'j', NULL),
(NULL, NULL, NULL, 'Diana', 'Price', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'price@symaptico.com', '1980-08-22', 'j', NULL),
(NULL, NULL, NULL, 'Adam', 'Jurris','98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', '1995-01-31', 'j', NULL),
(NULL, NULL, NULL, 'Judy', 'Sleen', '56343 Rover Ct.', 'Billings', 'MT', 672048823, 'jsleen@symaptico.com', '1970-03-22', 'j', NULL),
(NULL, NULL, NULL, 'Bill', 'Neiderheim', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', '1982-03-13', 'j', NULL);
COMMIT;

-- phone table -------
Start transaction;

insert into phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
values
(null, 1, 3098335221, 'h', null),
(null, 2, 0012322601, 'c', null),
(null, 3, 001233601, 'w', null),
(null, 4, 0937111091, 'c', 'needs for transfers'),
(null, 5, 2612261581, 'f', null),
(null, 6, 9009333111, 'w', 'call M,W,F'),
(null, 7, 8202442203, 'h', 'prefers home calls'),
(null, 8, 4998558294, 'w', null),
(null, 9, 7601661912, 'w', null),
(null, 10, 006771922, 'h', 'call M,W,F'),
(null, 11, 4508827902, 'w', 'prefers calls after buisness hours'),
(null, 12, 78699152617, 'c', 'needs for work(cell)'),
(null, 13, 3099919273, 'w', 'call M,W,F'),
(null, 14, 3710045902, 'w', 'needs new phone'),
(null, 15, 9000012345, 'w', 'needs for transfers');

commit;

-- client table -------
Start transaction;

insert into client
(per_id, cli_notes)
values
(1, null),
(2, null),
(3, null),
(4, null),
(5, null);

commit;

-- attorney table -------
Start transaction;

insert into attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
values
(6, '2022-04-13', null, 33, 6, null),
(7, '2011-03-24', null, 145, 2,null),
(8, '2012-11-15', null, 55, 5,null),
(9, '2013-03-26', null, 44, 10,null),
(10, '2014-04-27', null, 33, 21,null);

commit;

-- bar table -------
Start transaction;

insert into bar
(bar_id, per_id, bar_name, bar_notes)
values
(null, 6, 'Florida', null),
(null, 7, 'Colorado', null),
(null, 8, 'Tenessee', null),
(null, 9, 'Florida', null),
(null, 10, 'North Carolina', null),
(null, 6, 'Florida', null),
(null, 7, 'Utah', null),
(null, 8, 'North Dakota', null),
(null, 9, 'New Jersy', null),
(null, 10, 'New York', null),
(null, 6, 'Florida', null),
(null, 7, 'Virginia', null),
(null, 8, 'Florida', null),
(null, 9, 'Hawaii', null),
(null, 10, 'Florida', null),
(null, 6, 'Idaho', null),
(null, 7, 'Florida', null),
(null, 8, 'Florida', null),
(null, 9, 'Kansas', null);

commit;

-- specialty table -------
Start transaction;

insert into specialty
(spc_id, per_id, spc_type, spc_notes)
values
(null, 6, 'Judicial', null),
(null, 7, 'Judicial', null),
(null, 8, 'Criminal', null),
(null, 9, 'Judicial', null),
(null, 10, 'Criminal', null),
(null, 6, 'Criminal', null),
(null, 7, 'Murder', null),
(null, 8, 'Cold cases', null),
(null, 9, 'Criminal', null);

commit;

-- court table -------
Start transaction;

insert into court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
values
(null, 'Miami Traffic Court', '300 S 42nd Ave', 'Miami', 'FL', 323092162, 3056061234, 'MTC@court.com', 'https://www.miamitrafficcourt.com', null),
(null, 'Broward County Court of affairs', '200 W 43 St.', 'Ft. Lauderdale', 'FL', 898989292, 8891234100, 'BCCA@court.com', 'https://www.BrowardCountyCourtofaffairs.com', null),
(null, 'Orlando judicial courthouse', '500 N Capitol Rd.', 'Orlando', 'FL', 902315292, 8500098265, 'OJC@court.com', 'https://www.Orlandojudicialcourthouse.com', null),
(null, 'Jacksonville Judicial courthouse', '420 Smokey Blvd.', 'Jacksonville', 'FL', 309182148, 1234362000, 'JJC@court.com', 'https://www.JacksonvilleJudicialcourthouse.com', null),
(null, 'Tallahasse Dicstric of appeals', '900 S Monroe St.', 'Tallahassee', 'FL', 320923463, 3000888800, 'TDA@court.com', 'https://www.TallahasseDicstricofappeals.com', null);

commit;

-- judge table -------
Start transaction;

insert into judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
values
(11, 4, 130000, 9, null),
(12, 3, 140000, 5, null),
(13, 2, 130000, 7, null),
(14, 4, 150000, 9, null),
(15, 1, 260000, 11, null);

commit;

-- judge_hist table -------
Start transaction;

insert into judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
(null, 11, 2, '2013-02-15', 'i', 100010, null),
(null, 12, 3, '2012-03-13', 'i', 100020, null),
(null, 13, 3, '2005-04-09', 'i', 120030, null),
(null, 13, 4, '2014-05-18', 'i', 125040, null),
(null, 14, 2, '2013-12-27', 'i', 125050, null),
(null, 15, 3, '2015-12-16', 'i', 125060, 'First Female!'),
(null, 11, 2, '2006-12-15', 'i', 180070, 'Paydock for criminal activity'),
(null, 12, 1, '2012-06-14', 'i', 185070, 'Great Service over 10 years'),
(null, 14, 3, '2018-06-12', 'i', 180080, 'Rasie for Great Service overe 10 years');

commit;

-- `case` table -------
Start transaction;

insert into `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
values
(NULL, 13, 'civil', 'client says that his logo is being used without his consent to promote a rival business', '2010-09-09', NULL, 'copyright infringement'),
(NULL, 12, 'criminal', 'client is charged with assaulting her husband during an argument', '2009-11-18', '2010-12-23', 'assault'),
(NULL, 14, 'civil', 'client broke an ankle while shopping at a local grocery store. no wet floor sign was posted althoug the floor had just been mopped', '2008-05-06', '2008-07-23', 'slip and fall'),
(NULL, 11, 'criminal', 'client was charged with stealing several televisions from his former place of employment. client has a solid alibi', '2011-05-20', NULL, 'grand theft'),
(NULL, 13, 'criminal', 'client charged with posession of 10 grams of cocaine, allegedly found in his glove box by state police', '2011-06-05', NULL, 'posession of narcotics'),
(NULL, 14, 'civil', 'client alleges newspaper printed false information about his personal activities while he ran a large laundry business in a small nearby town.', '2007-01-19', '2007-05-20', 'defamation'),
(NULL, 12, 'criminal', 'client charged with the murder of his co-worker over a lovers fued. client has no alibi', '2010-03-20', NULL, 'murder'),
(NULL, 15, 'civil', 'client made the horrible mistake of selecting a degree other than IT and had to declare bankruptcy.', '2012-01-26', '2013-02-28', 'bankruptcy');

commit;

-- assignment table -------
START TRANSACTION;

INSERT INTO assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
VALUES
(NULL, 1, 6, 7, NULL),
(NULL, 2, 6, 6, NULL),
(NULL, 3, 7, 2, NULL),
(NULL, 4, 8, 2, NULL),
(NULL, 5, 9, 5, NULL),
(NULL, 1, 10, 1, NULL),
(NULL, 2, 6, 3, NULL),
(NULL, 3, 7, 8, NULL),
(NULL, 4, 8, 8, NULL),
(NULL, 5, 9, 8, NULL),
(NULL, 4, 10, 4, NULL);
COMMIT;